/**
 * Created on 2019-07-08.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { get } from 'lodash';

export function getEnv<T>(key: string, defaultValue: T): T {
    return get(process.env, key, defaultValue) as T;
}
