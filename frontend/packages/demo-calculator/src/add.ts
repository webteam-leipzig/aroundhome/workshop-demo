/**
 * Created on 2019-07-08.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

/**
 * Adds one or more values
 *
 * @param {number} first : The first value
 * @param {number[]} args : The values to add
 * @return {number} : The sum
 */
export function add(first: number, ...args: number[]): number {
    return args.reduce((accumulator: number, current: number) => accumulator + current, first || 0);
}
