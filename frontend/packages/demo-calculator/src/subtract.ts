/**
 * Created on 2019-07-08.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

/**
 * Subtracts one or more values
 *
 * @param {number} first : The first value
 * @param {number[]} args : The values to subtract
 * @return {number} : The result
 */
export function subtract(first: number, ...args: number[]): number {
    return args.reduce((accumulator: number, current: number) => accumulator - current, first || 0);
}
