
// This is a custom Jest transformer turning style imports into empty objects.
// http://facebook.github.io/jest/docs/en/webpack.html

module.exports = {
    /**
     * @return {string}
     */
    process() {
        return 'module.exports = {};';
    },
    /**
     * @return {string}
     */
    getCacheKey() {
    // The output is always the same.
        return 'cssTransform';
    },
};
