/**
 * Created on 2019-07-08.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { get } from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Dispatch } from 'redux';
import { ICalculationHelpers, initialState as initialCalculationState, withCalculation } from '../../../shared/calculation';

interface IAddPageProps extends ICalculationHelpers, RouteComponentProps<{ count?: string; }> {
    dispatch: Dispatch;
    value: number;
}

/**
 * @class SubtractPage
 */
export class AddPage extends Component<IAddPageProps> {
    /**
     * Renders the <AddPage/> component
     *
     * @return {JSX.Element}: The AddPage component
     */
    public render(): JSX.Element {
        return (
            <div>
                <h1>Add some values to {this.props.value}.</h1>
                <button onClick={this.handleAddButtonClick}>Add</button>
            </div>
        );
    }

    /**
     * Handles the click on the add button
     */
    private handleAddButtonClick = () => {
        let count = 1;
        try {
            if (this.props.match.params.count) {
                count = parseInt(this.props.match.params.count, 10);
            }
        } catch (e) { /**/ }

        this.props.addValue(count);
    }
}

export default connect(state => ({
    value: get(state, 'calculation.value', initialCalculationState.value)
}))(withCalculation(AddPage));
