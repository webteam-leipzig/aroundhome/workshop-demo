/**
 * Created on 2019-07-08.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { get } from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Dispatch } from 'redux';
import {
    ICalculationHelpers,
    initialState as initialCalculationState,
    withCalculation
} from '../../../shared/calculation';

interface ISubtractPageProps extends ICalculationHelpers, RouteComponentProps<{ count?: string; }> {
    dispatch: Dispatch;
    value: number;
    isPreview: boolean;
}

/**
 * @class SubtractPage
 */
export class SubtractPage extends Component<ISubtractPageProps> {
    private readonly subtractPreviewValue = 5;

    /**
     * Renders the <SubtractPage/> component
     *
     * @return {JSX.Element}: The SubtractPage component
     */
    public render(): JSX.Element {
        return (
            <div>
                <h1>Subtract some values from {this.props.value}.</h1>
                <button onClick={this.handleSubtractButtonClick}>Subtract</button>
                <button onClick={this.handleSubtractPreviewButtonClick}>
                    Subtract Preview of {this.subtractPreviewValue}
                </button>
                { this.props.isPreview &&
                    <button onClick={this.props.takeSubtractionPreview}>
                        Accept Preview
                    </button>
                }
            </div>
        );
    }

    /**
     * Handles the click on the subtract button
     */
    private handleSubtractButtonClick = () => {
        let count = 1;
        try {
            if (this.props.match.params.count) {
                count = parseInt(this.props.match.params.count, 10);
            }
        } catch (e) { /**/ }

        this.props.subtractValue(count);
    }

    /**
     * Handles the click on the subtract preview button
     */
    private handleSubtractPreviewButtonClick = () => {
        this.props.previewSubtractionOf(this.subtractPreviewValue, 10);
    }
}

export default connect(state => ({
    value: get(state, 'calculation.value', initialCalculationState.value),
    isPreview: get(state, 'calculation.isPreview', initialCalculationState.isPreview),
}))(withCalculation(SubtractPage));
