/**
 * Created on 2019-07-08.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { getEnv } from '@demo/utils';
import i18next from 'i18next';
// import Backend from 'i18next-xhr-backend';
import i18nextBrowserLanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';
import deDEJson from '../lang/de-DE.json';
import enUSJson from '../lang/en-US.json';

i18next
// load translation using xhr -> see /public/locales
// learn more: https://github.com/i18next/i18next-xhr-backend
// .use(Backend)
// detect user language
// learn more: https://github.com/i18next/i18next-browser-languageDetector
    .use(i18nextBrowserLanguageDetector)
    // pass the i18n instance to react-i18next.
    .use(initReactI18next)
    // init i18next
    // for all options read: https://www.i18next.com/overview/configuration-options
    .init({
        fallbackLng: 'de-DE',
        debug: getEnv('NODE_ENV', 'development') === 'development',
        resources: {
            'de-DE': { translation: deDEJson },
            'en-GB': { translation: enUSJson },
            'en-US': { translation: enUSJson },
        },

        interpolation: {
            escapeValue: false, // not needed for react as it escapes by default
        },
    });

export default i18next;
