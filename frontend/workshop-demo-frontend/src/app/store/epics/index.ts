/**
 * Created on 2019-07-08.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { values } from 'lodash';
import { Epic } from 'redux-observable';
import { epics as calculationEpics } from '../../../shared/calculation';

export const epics: Epic[] = [
    ...values(calculationEpics),
];
