/**
 * Created on 2019-07-08.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
import { initialState as calculation } from '../../../shared/calculation/store/state';

export const initialState = {
    calculation,
};
