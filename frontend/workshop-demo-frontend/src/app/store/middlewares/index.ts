/**
 * Created on 2019-07-08.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { Middleware } from 'redux';
import thunk from 'redux-thunk';

export const middlewares: Middleware[] = [
    thunk,
];
