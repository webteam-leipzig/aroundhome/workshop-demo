/**
 * Created on 2019-07-08.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
import { calculationReducer } from '../../../shared/calculation';

export const reducers = {
    calculation: calculationReducer,
};
