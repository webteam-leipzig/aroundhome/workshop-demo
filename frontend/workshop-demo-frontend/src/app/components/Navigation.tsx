/**
 * Created on 2019-07-31.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import React from 'react';
import { NavLink } from 'react-router-dom';

export const Navigation = () => {
    return (
        <ul className="navigation">
            <li><NavLink className="navigation__link" to="/add">To Add</NavLink></li>
            <li><NavLink className="navigation__link" to="/subtract">To Subtract</NavLink></li>
        </ul>
    )
};
