/**
 * Created on 2019-07-08.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { History } from 'history';
import React, { Component } from 'react';
import { Redirect, Route, Router, Switch } from 'react-router-dom';
import { routes } from '../routes';
import { Navigation } from './Navigation';

/**
 * The props of ApplicationRouter
 */
export interface IApplicationRouterProps {
    history: History;
}

/**
 * @class ApplicationRouter
 */
export class ApplicationRouter extends Component<IApplicationRouterProps> {
    /**
     * Renders the <ApplicationRouter/> component
     *
     * @return {JSX.Element}: The ApplicationRouter component
     */
    public render(): JSX.Element {
        return (
            <Router history={this.props.history}>
                <Navigation/>
                <Switch>
                    {routes.map((route, key) => {
                        return <Route key={key} {...route}/>
                    })}
                    <Redirect to="/add" />
                </Switch>
            </Router>
        );
    }
}
