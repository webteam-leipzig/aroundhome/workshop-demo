/**
 * Created on 2019-07-08.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { getEnv } from '@demo/utils';
import { connectRouter, routerMiddleware, RouterState } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import React from 'react';
import { Provider } from 'react-redux';
import { AnyAction, applyMiddleware, combineReducers, compose, createStore, Reducer, Store } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { combineEpics, createEpicMiddleware } from 'redux-observable';
import { epics } from '../../store/epics';
import { middlewares } from '../../store/middlewares';
import { reducers } from '../../store/reducer';
import { initialState } from '../../store/state';
import { ApplicationRouter } from '../ApplicationRouter';

const epicMiddleware = createEpicMiddleware();
const rootEpic = combineEpics(...epics);

const history = createBrowserHistory();
const enhancers = [
    applyMiddleware(
        ...middlewares,
        routerMiddleware(history),
        epicMiddleware,
    ),
];

const store = createStore(
    combineReducers({
        ...reducers,
        router: connectRouter(history) as Reducer<RouterState, AnyAction>,
    }),
    initialState,
    getEnv('NODE_ENV', 'development') === 'development'
        ? composeWithDevTools(...enhancers)
        : compose(...enhancers),
);
epicMiddleware.run(rootEpic);

/**
 * Attaches the provider to the application and renders the router
 * @return {any}
 * @constructor
 */
export const ReduxWrapper = () => (
    <Provider store={store as Store}>
        <ApplicationRouter history={history} />
    </Provider>
);
