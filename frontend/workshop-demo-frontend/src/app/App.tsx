import React from 'react';
import { ReduxWrapper } from './components/wrapper/ReduxWrapper';

const App: React.FC = () => {
    return <ReduxWrapper/>;
};

export default App;
