/**
 * Created on 2019-07-08.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { RouteProps } from 'react-router';
import AddPage from '../pages/add/components/AddPage';
import SubtractPage from '../pages/subtract/components/SubtractPage';

export const routes: RouteProps[] = [
    {
        path: '/add/:count?',
        component: AddPage,
    },
    {
        path: '/subtract/:count?',
        component: SubtractPage,
    },
];
