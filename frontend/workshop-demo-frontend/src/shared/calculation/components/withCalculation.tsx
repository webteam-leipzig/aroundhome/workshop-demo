/**
 * Created on 2019-07-31.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import React, { ComponentType } from 'react';
import { Dispatch } from 'redux';
import {
    addValueAction,
    subtractValueAction,
    subtractValuePreviewAcceptAction,
    subtractValuePreviewAction
} from '../store/actions';

export interface ICalculationHelpers {
    addValue: (value: number) => void;
    subtractValue: (value: number) => void;
    previewSubtractionOf: (value: number, forSeconds: number) => void;
    takeSubtractionPreview: () => void;
}

/**
 * Adds calculation helpers
 */
export function withCalculation<T extends { dispatch: Dispatch }>(
    WrappedComponent: ComponentType<T>,
) {
    return (props: T) => {
        function addValue(value: number) {
            props.dispatch(addValueAction(value));
        }

        function subtractValue(value: number) {
            props.dispatch(subtractValueAction(value));
        }

        function previewSubtractionOf(value: number, forSeconds: number) {
            props.dispatch(subtractValuePreviewAction(value, forSeconds));
        }

        function takeSubtractionPreview() {
            props.dispatch(subtractValuePreviewAcceptAction());
        }

        return <WrappedComponent
            {...props as T}
            addValue={addValue}
            subtractValue={subtractValue}
            previewSubtractionOf={previewSubtractionOf}
            takeSubtractionPreview={takeSubtractionPreview}
        />
    }
}
