/**
 * Created on 2019-07-31.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
import { add, subtract } from '@demo/calculator';
import { AnyAction } from 'redux';
import { IAddValueAction, ISubtractValueAction } from '../actions';
import {
    ADD_VALUE,
    SUBTRACT_VALUE,
    SUBTRACT_VALUE_PREVIEW,
    SUBTRACT_VALUE_PREVIEW_ACCEPT,
    SUBTRACT_VALUE_PREVIEW_RESET,
} from '../actions/types';
import { ICalculationState, initialState } from '../state';

export function calculationReducer(state: ICalculationState = initialState, action: AnyAction): ICalculationState {
    switch (action.type) {
        case ADD_VALUE:
            return {
                ...state,
                value: add(state.value, (action as IAddValueAction).payload.value),
            };
        case SUBTRACT_VALUE_PREVIEW_RESET:
            return {
                ...state,
                value: add(state.value, (action as IAddValueAction).payload.value),
                isPreview: false,
            };
        case SUBTRACT_VALUE_PREVIEW:
            return {
                ...state,
                isPreview: true,
                value: subtract(state.value, (action as ISubtractValueAction).payload.value),
            };
        case SUBTRACT_VALUE_PREVIEW_ACCEPT:
            return {
                ...state,
                isPreview: false,
            };
        case SUBTRACT_VALUE:
            return {
                ...state,
                value: subtract(state.value, (action as ISubtractValueAction).payload.value),
            };
        default:
            return state;
    }
}
