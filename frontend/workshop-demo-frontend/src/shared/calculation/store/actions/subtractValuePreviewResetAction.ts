/**
 * Created on 2019-07-31.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
import { IAddValueAction } from './addValueAction';
import { SUBTRACT_VALUE_PREVIEW_RESET } from './types';

export interface ISubtractValuePreviewResetAction extends Omit<IAddValueAction, 'type'> {
    type: typeof SUBTRACT_VALUE_PREVIEW_RESET;
}

export function subtractValuePreviewResetAction(value: number): ISubtractValuePreviewResetAction {
    return {
        type: SUBTRACT_VALUE_PREVIEW_RESET,
        payload: {
            value,
        },
    };
}
