/**
 * Created on 2019-07-31.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
import { ADD_VALUE } from './types';

export interface IAddValueAction {
    type: typeof ADD_VALUE;
    payload: {
        value: number;
    };
}

export function addValueAction(value: number = 1): IAddValueAction {
    return {
        type: ADD_VALUE,
        payload: {
            value,
        },
    };
}
