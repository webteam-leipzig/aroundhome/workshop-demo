/**
 * Created on 2019-07-31.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
import { SUBTRACT_VALUE } from './types';

export interface ISubtractValueAction {
    type: typeof SUBTRACT_VALUE;
    payload: {
        value: number;
    };
}

export function subtractValueAction(value: number = 1): ISubtractValueAction {
    return {
        type: SUBTRACT_VALUE,
        payload: {
            value,
        },
    };
}
