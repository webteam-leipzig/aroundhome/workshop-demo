/**
 * Created on 2019-07-31.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
import { SUBTRACT_VALUE_PREVIEW_ACCEPT } from './types';

export interface ISubtractValuePreviewAcceptAction {
    type: typeof SUBTRACT_VALUE_PREVIEW_ACCEPT;
}

export function subtractValuePreviewAcceptAction(): ISubtractValuePreviewAcceptAction {
    return {
        type: SUBTRACT_VALUE_PREVIEW_ACCEPT,
    };
}
