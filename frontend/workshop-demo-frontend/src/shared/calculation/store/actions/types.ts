/**
 * Created on 2019-07-31.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

export const ADD_VALUE = 'ADD_VALUE';
export const SUBTRACT_VALUE = 'SUBTRACT_VALUE';
export const SUBTRACT_VALUE_PREVIEW = 'SUBTRACT_VALUE_PREVIEW';
export const SUBTRACT_VALUE_PREVIEW_ACCEPT = 'SUBTRACT_VALUE_PREVIEW_ACCEPT';
export const SUBTRACT_VALUE_PREVIEW_RESET = 'SUBTRACT_VALUE_PREVIEW_RESET';
