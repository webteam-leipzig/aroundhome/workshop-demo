/**
 * Created on 2019-07-31.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
import { ISubtractValueAction } from './subtractValueAction';
import { SUBTRACT_VALUE_PREVIEW } from './types';

export interface ISubtractValuePreviewAction extends Omit<ISubtractValueAction, 'type'> {
    type: typeof SUBTRACT_VALUE_PREVIEW;
    previewSeconds: number;
}

export function subtractValuePreviewAction(
    value: number = 1,
    previewSeconds: number = 5,
): ISubtractValuePreviewAction {
    return {
        previewSeconds,
        type: SUBTRACT_VALUE_PREVIEW,
        payload: {
            value,
        },
    };
}
