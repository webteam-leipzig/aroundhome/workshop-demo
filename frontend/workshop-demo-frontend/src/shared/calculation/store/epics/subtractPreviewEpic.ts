/**
 * Created on 2019-07-31.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
import { ActionsObservable, Epic } from 'redux-observable';
import { Observable, of } from 'rxjs';
import { delay, map, mergeMap, takeUntil, } from 'rxjs/operators';
import { ISubtractValuePreviewAction, subtractValuePreviewResetAction } from '../actions';
import { SUBTRACT_VALUE_PREVIEW, SUBTRACT_VALUE_PREVIEW_ACCEPT } from '../actions/types';

export const subtractPreviewEpic: Epic = (action$: ActionsObservable<ISubtractValuePreviewAction>): Observable<any> => {
    return action$.ofType(SUBTRACT_VALUE_PREVIEW).pipe(
        mergeMap((sourceAction) => of(sourceAction).pipe(
            delay(sourceAction.previewSeconds * 1000),
            map((action) => {
                return subtractValuePreviewResetAction(action.payload.value);
            }),
            takeUntil(action$.ofType(SUBTRACT_VALUE_PREVIEW_ACCEPT)),
        )),
    );
};
