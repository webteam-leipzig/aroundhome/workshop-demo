/**
 * Created on 2019-07-31.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { subtractPreviewEpic } from './subtractPreviewEpic';

export const epics = {
    subtractPreviewEpic,
};
