/**
 * Created on 2019-07-31.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

export interface ICalculationState {
    value: number;
    isPreview: boolean;
}

export const initialState: ICalculationState = {
    value: 0,
    isPreview: false,
};
