/**
 * Created on 2019-07-08.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

const path = require('path');
const fs = require('fs');

const appDirectory = fs.realpathSync(process.cwd());
const buildDirectoryName = 'dist';
const buildDirectory = path.resolve(process.cwd(), `./${buildDirectoryName}`);

/**
 * Resolves a relative path to the app directory
 *
 * @param {string} relativePath : String : The relative path to resolve
 * @return {string} : The absolute path to the app directory
 */
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

/**
 * Resolves a relative path to the build directory
 *
 * @param {string} relativePath : String : The relative path to resolve
 * @return {string} : The absolute path to the build directory
 */
const resolveBuild = relativePath => path.resolve(buildDirectory, relativePath);

module.exports = {
    buildCSS: resolveBuild('css'),
    buildImage: resolveBuild('img'),
    buildFont: resolveBuild('fonts'),
    appPackageJson: resolveApp('package.json'),
    appNodeModules: resolveApp('node_modules'),
    appStyle: resolveApp('style'),
    appStyleIndex: resolveApp('style/index.scss'),
    appImage: resolveApp('img'),
    appFont: resolveApp('fonts'),
    appDirectory,
    buildDirectory,
};
