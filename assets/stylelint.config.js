module.exports = {
    extends: 'stylelint-config-standard',
    plugins: [
        'stylelint-no-unsupported-browser-features',
    ],
    rules: {
        indentation: 4,
        // using autoprefixer, so disallow vendor prefixes
        'at-rule-no-vendor-prefix': true,
        'media-feature-name-no-vendor-prefix': true,
        'property-no-vendor-prefix': true,
        'selector-no-vendor-prefix': true,
        'value-no-vendor-prefix': true,
        'max-line-length': [
            120,
            {
                ignore: ['comments'],
            },
        ],
        'plugin/no-unsupported-browser-features': true,
    },
};
