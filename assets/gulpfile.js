const { dest, parallel, series, src, task, watch } = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const cleanCSS = require('gulp-clean-css');
const postcss = require('gulp-postcss');
const clean = require('gulp-clean');
const autoprefixer = require('autoprefixer');
const postcssFlexibility = require('postcss-flexibility');
const gulpStylelint = require('gulp-stylelint');
const paths = require('./paths');

task('clean:style', () => {
    return src([`${paths.buildCSS}/*`, `!${paths.buildCSS}/.gitignore`], { read: false })
        .pipe(clean({ force: true }))
});
task('clean:fonts', () => {
    return src([`${paths.buildFont}/*`, `!${paths.buildFont}/.gitignore`], { read: false })
        .pipe(clean({ force: true }))
});
task('clean:images', () => {
    return src([`${paths.buildImage}/*`, `!${paths.buildImage}/.gitignore`], { read: false })
        .pipe(clean({ force: true }))
});
task('clean', parallel(task('clean:images'), task('clean:fonts'), task('clean:style')));

task('style', () => {
    return src(paths.appStyleIndex)
        .pipe(gulpStylelint({
            reporters: [
                { formatter: 'string', console: true }
            ]
        }))
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([ autoprefixer, postcssFlexibility ]))
        .pipe(sourcemaps.write())
        .pipe(dest(paths.buildCSS))
});

task('minify', () => {
    return src(`${paths.buildCSS}/**/*.css`)
        .pipe(cleanCSS())
        .pipe(dest(paths.buildCSS))
});

task('images', () => {
    return src(`${paths.appImage}/**/*`)
        .pipe(dest(paths.buildImage))
});

task('fonts', () => {
    return src(`${paths.appFont}/**/*`)
        .pipe(dest(paths.buildFont))
});

task('watch:fonts', () => watch(`${paths.appFont}/**/*`, task('fonts')));
task('watch:images', () => watch(`${paths.appImage}/**/*`, task('images')));
task('watch:style', () => watch(`${paths.appStyle}/**/*`, task('style')));

task('watch', series(
    task('clean'),
    parallel(task('images'), task('fonts'), task('style')),
    parallel(
        task('watch:fonts'),
        task('watch:images'),
        task('watch:style'),
    ),
));

task('build', series(
    task('clean'),
    parallel(task('images'), task('fonts'), task('style')),
    task('minify'),
));
